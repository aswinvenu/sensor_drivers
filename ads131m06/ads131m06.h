/*
 * ads131m06.h
 *
 *  Created on: Nov 5, 2023
 *      Author: aswin
 */
#include <stdint.h>
#include "stm32mp1xx_hal.h"

#ifndef INC_ADS131M06_H_
#define INC_ADS131M06_H_

/* Registers */
#define ID	0x00
#define STATUS	0x01

#define MODE	0x02
#define CLOCK	0x03
#define GAIN1	0x04
#define GAIN2	0x05
#define CFG	0x06
#define THRSHLD_MSB	0x07
#define THRSHLD_LSB	0x08
/* Channel 0 */
#define CH0_CFG	0x09
#define CH0_OCAL_MSB	0x0A
#define CH0_OCAL_LSB	0x0B
#define CH0_GCAL_MSB	0x0C
#define CH0_GCAL_LSB	0x0D
/* Channel 1 */
#define CH1_CFG	0x0E
#define CH1_OCAL_MSB	0x0F
#define CH1_OCAL_LSB	0x10
#define CH1_GCAL_MSB	0x11
#define CH1_GCAL_LSB	0x12
/* Channel 2 */
#define CH2_CFG	0x13
#define CH2_OCAL_MSB	0x14
#define CH2_OCAL_LSB	0x15
#define CH2_GCAL_MSB	0x16
#define CH2_GCAL_LSB	0x17
/* Channel 3 */
#define CH3_CFG	0x18
#define CH3_OCAL_MSB	0x19
#define CH3_OCAL_LSB	0x1A
#define CH3_GCAL_MSB	0x1B
#define CH3_GCAL_LSB	0x1C
/* Channel 4 */
#define CH4_CFG	0x1D
#define CH4_OCAL_MSB	0x1E
#define CH4_OCAL_LSB	0x1F
#define CH4_GCAL_MSB	0x20
#define CH4_GCAL_LSB	0x21
/* Channel 5 */
#define CH5_CFG	0x22
#define CH5_OCAL_MSB	0x23
#define CH5_OCAL_LSB	0x24
#define CH5_GCAL_MSB	0x25
#define CH5_GCAL_LSB	0x26

#define REGMAP	0x3E

/* COMMANDS */
#define CMD_NULL	0x0000
#define CMD_RESET	0x0011
#define CMD_STANDBY	0x0022
#define CMD_WAKEUP	0x0033
#define CMD_LOCK	0x0555
#define CMD_UNLOCK	0x0666

#define RREG	5 << 13
#define WREG	3 << 13

#define ADC_FRAME_LENGTH	12
#define ADS131M06_CS_GPIO_PORT	GPIOF
#define ADS131M06_CS_GPIO_PIN	GPIO_PIN_6

#define ADS131M06_DRDY_GPIO_PORT	GPIOF
#define ADS131M06_DRDY_GPIO_PIN	GPIO_PIN_4

/* Sampling rate options @ 8.192MHz */
#define ADC_SAMPLING_32KSPS	0
#define ADC_SAMPLING_16KSPS	1 << 2
#define ADC_SAMPLING_8KSPS	2 << 2
#define ADC_SAMPLING_4KSPS	3 << 2
#define ADC_SAMPLING_2KSPS	4 << 2
#define ADC_SAMPLING_1KSPS	5 << 2
#define ADC_SAMPLING_500SPS	6 << 2
#define ADC_SAMPLING_250SPS	7 << 2

#define ADC_CH1_EN	1 << 13
#define ADC_CH2_EN	1 << 12
#define ADC_CH3_EN	1 << 11
#define ADC_CH4_EN	1 << 10
#define ADC_CH5_EN	1 << 9
#define ADC_CH6_EN	1 << 8

#define ADC_XTAL_DISABLE 1 << 7
#define ADC_HIGH_RES 1 << 1

#define ADC_GAIN_1	0
#define ADC_GAIN_2	1
#define ADC_GAIN_4	2
#define ADC_GAIN_8	3
#define ADC_GAIN_16	4
#define ADC_GAIN_32	5
#define ADC_GAIN_64	6
#define ADC_GAIN_128	7

/* GAIN 1 Reg */
#define ADC_CHANNEL_1	0
#define ADC_CHANNEL_2	4
#define ADC_CHANNEL_3	8
#define ADC_CHANNEL_4	12
/* GAIN 2 Reg */
#define ADC_CHANNEL_5	0
#define ADC_CHANNEL_6	4

struct adc_data {
	uint8_t ch1_data[3];
	uint8_t ch2_data[3];
	uint8_t ch3_data[3];
	uint8_t ch4_data[3];
	uint8_t ch5_data[3];
	uint8_t ch6_data[3];
};

/* Function declarations */
int ads131m06_readData(SPI_HandleTypeDef *adc_spi, uint16_t *data);
int ads131m06_adcInit(SPI_HandleTypeDef *adc_spi);
void ads131m06_enableEXTI(void);
void ads131m06_disableEXTI(void);
int ads131m06_hardwareInit(SPI_HandleTypeDef *adc_spi);

#endif /* INC_ADS131M06_H_ */

