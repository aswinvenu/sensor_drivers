/*
 * ads131m06.c
 *
 *  Created on: Nov 5, 2023
 *      Author: Aswin Venu
 */


#include "ads131m06.h"

uint16_t spi_tx[ADC_FRAME_LENGTH], spi_rx[ADC_FRAME_LENGTH];

static int ads131m06_sendCmd(SPI_HandleTypeDef *adc_spi, uint16_t cmd)
{

	//spi_tx[0] = (cmd >> 8) | (cmd << 8);
	spi_tx[0] = cmd;
	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 0);
	/* send the read register command */
	HAL_SPI_Transmit(adc_spi, (uint8_t*)&spi_tx, ADC_FRAME_LENGTH, 1);
	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 1);

	return 0;
}

static int ads131m06_readReg(SPI_HandleTypeDef *adc_spi, uint16_t regAddr, uint16_t* regVal)
{

	spi_tx[0] = RREG | (regAddr << 7);

	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 0);
	/* send the read register command */
	HAL_SPI_TransmitReceive(adc_spi, (uint8_t*)&spi_tx, (uint8_t*)&spi_rx, ADC_FRAME_LENGTH, 1);
	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 1);

	spi_tx[0] = CMD_NULL;
	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 0);
	/* send a null command to receive the data */
    HAL_SPI_TransmitReceive(adc_spi, (uint8_t*)&spi_tx, (uint8_t*)&spi_rx, ADC_FRAME_LENGTH, 1);
	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 1);
    /* Result */
	*regVal = *spi_rx;

	return 0;
}

static int ads131m06_writeReg(SPI_HandleTypeDef *adc_spi, uint16_t regAddr, uint16_t regVal)
{
	spi_tx[0] = WREG | (regAddr << 7);
	/* Register value to write */
	spi_tx[1] = 0x00FF & (regVal >> 8);
	spi_tx[2] = (regVal << 8) & 0xFF00;


	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 0);
	/* send the read register command */
	HAL_SPI_TransmitReceive(adc_spi, (uint8_t*)&spi_tx, (uint8_t*)&spi_rx, ADC_FRAME_LENGTH, 1);
	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 1);

	spi_tx[0] = CMD_NULL;
	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 0);
	/* send a null command to complete the data transfer */
	HAL_SPI_TransmitReceive(adc_spi, (uint8_t*)&spi_tx, (uint8_t*)&spi_rx, ADC_FRAME_LENGTH, 1);
	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 1);
	HAL_Delay(1);
	return 0;
}

int ads131m06_readData(SPI_HandleTypeDef *adc_spi, uint16_t *data)
{

	spi_tx[0] = CMD_NULL;
	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 0);
	/* send the read register command */
	HAL_SPI_TransmitReceive(adc_spi, (uint8_t*)&spi_tx, (uint8_t*)data, ADC_FRAME_LENGTH, 1);
	HAL_GPIO_WritePin(ADS131M06_CS_GPIO_PORT, ADS131M06_CS_GPIO_PIN, 1);
	return 0;
}

int ads131m06_adcInit(SPI_HandleTypeDef *adc_spi)
{
	int ret;
	uint16_t reg;
	/* Reset the ADC */
	ads131m06_sendCmd(adc_spi, CMD_RESET);
	/* Blocking delay */
	HAL_Delay(10);
	/* Set the device in standby mode */
	ads131m06_sendCmd(adc_spi, CMD_STANDBY);
	/* Read the ID and verifies the CHIP */
	ads131m06_readReg(adc_spi, ID , &reg);
	/* Enable the channels */
	reg = (ADC_CH1_EN | ADC_CH2_EN | ADC_CH3_EN | ADC_CH4_EN | ADC_CH5_EN | ADC_CH6_EN);
	/* Change the sampling rate to 32K SPS*/
	reg |= ADC_SAMPLING_32KSPS | ADC_XTAL_DISABLE | ADC_HIGH_RES;
	ads131m06_writeReg(adc_spi, CLOCK, reg);
	reg = 0;
	/* Set the gain here : REG 1*/
	reg = ((ADC_GAIN_1 << ADC_CHANNEL_1) | (ADC_GAIN_1 << ADC_CHANNEL_2) |
			(ADC_GAIN_1 << ADC_CHANNEL_3) | (ADC_GAIN_1 << ADC_CHANNEL_4));
	ads131m06_writeReg(adc_spi, GAIN1, reg);
	/* REG2 */
	reg = 0;
	reg = ((ADC_GAIN_1 << ADC_CHANNEL_5) | (ADC_GAIN_1 << ADC_CHANNEL_6));
	ads131m06_writeReg(adc_spi, GAIN2, reg);
	/* Start the ADC Conversion */
	ads131m06_sendCmd(adc_spi, CMD_WAKEUP);
	return ret;

}

void ads131m06_enableEXTI()
{
	  HAL_NVIC_EnableIRQ(EXTI4_IRQn);
}

void ads131m06_disableEXTI()
{
	  HAL_NVIC_DisableIRQ(EXTI4_IRQn);
}

static int ads131m06_gpioInit()
{
	  GPIO_InitTypeDef GPIO_InitStruct = {0};

	  /* GPIO Ports Clock Enable */
	  __HAL_RCC_GPIOF_CLK_ENABLE();
	  __HAL_RCC_GPIOC_CLK_ENABLE();
	  __HAL_RCC_GPIOH_CLK_ENABLE();

	  /*Configure GPIO pin : PF4 */
	  GPIO_InitStruct.Pin = ADS131M06_DRDY_GPIO_PIN;
	  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  HAL_GPIO_Init(ADS131M06_DRDY_GPIO_PORT, &GPIO_InitStruct);

	  /* EXTI interrupt init */
	  HAL_NVIC_SetPriority(EXTI4_IRQn, 1, 0);
	  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

	  GPIO_InitStruct.Pin = ADS131M06_CS_GPIO_PIN;
	  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	  HAL_GPIO_Init(ADS131M06_CS_GPIO_PORT, &GPIO_InitStruct);
	  /* EXTI interrupt init */
	  HAL_NVIC_SetPriority(EXTI4_IRQn, 1, 0);
}

static int ads131m06_spiInit(SPI_HandleTypeDef *adc_spi)
{
	  int ret;
	  adc_spi->Instance = SPI5;
	  adc_spi->Init.Mode = SPI_MODE_MASTER;
	  adc_spi->Init.Direction = SPI_DIRECTION_2LINES;
	  adc_spi->Init.DataSize = SPI_DATASIZE_16BIT;
	  adc_spi->Init.CLKPolarity = SPI_POLARITY_LOW;
	  adc_spi->Init.CLKPhase = SPI_PHASE_2EDGE;
	  adc_spi->Init.NSS = SPI_NSS_SOFT;
	  adc_spi->Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
	  adc_spi->Init.FirstBit = SPI_FIRSTBIT_MSB;
	  adc_spi->Init.TIMode = SPI_TIMODE_DISABLE;
	  adc_spi->Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	  adc_spi->Init.CRCPolynomial = 0x0;
	  adc_spi->Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
	  adc_spi->Init.NSSPolarity = SPI_NSS_POLARITY_LOW;
	  adc_spi->Init.FifoThreshold = SPI_FIFO_THRESHOLD_01DATA;
	  adc_spi->Init.TxCRCInitializationPattern = SPI_CRC_INITIALIZATION_ALL_ZERO_PATTERN;
	  adc_spi->Init.RxCRCInitializationPattern = SPI_CRC_INITIALIZATION_ALL_ZERO_PATTERN;
	  adc_spi->Init.MasterSSIdleness = SPI_MASTER_SS_IDLENESS_00CYCLE;
	  adc_spi->Init.MasterInterDataIdleness = SPI_MASTER_INTERDATA_IDLENESS_00CYCLE;
	  adc_spi->Init.MasterReceiverAutoSusp = SPI_MASTER_RX_AUTOSUSP_DISABLE;
	  adc_spi->Init.MasterKeepIOState = SPI_MASTER_KEEP_IO_STATE_DISABLE;
	  adc_spi->Init.IOSwap = SPI_IO_SWAP_DISABLE;
	  if (HAL_SPI_Init(adc_spi) != HAL_OK)
	  {
	    return -1;
	  }
	  return 0;
}

int ads131m06_hardwareInit(SPI_HandleTypeDef *adc_spi)
{
	int ret;
	ads131m06_gpioInit();
	ret = ads131m06_spiInit(adc_spi);
	if(ret){
		return ret;
	}
	return 0;
}

