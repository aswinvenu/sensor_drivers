
ADS131M06 is a 24 bit 6 channel simultaneous sampling Sigma-Delta ADC from Texas Instruments

Note : This driver is written for STM32 series of micro-crontrollers ( USING Cube MX )
	But with minimal effort it can be ported to any uC platforms.

SPI Config is as follows

/* SPI5 parameter configuration*/
hspi5.Instance = SPI5;
hspi5.Init.Mode = SPI_MODE_MASTER;
hspi5.Init.Direction = SPI_DIRECTION_2LINES;
hspi5.Init.DataSize = SPI_DATASIZE_8BIT;
hspi5.Init.CLKPolarity = SPI_POLARITY_LOW;
hspi5.Init.CLKPhase = SPI_PHASE_2EDGE;
hspi5.Init.NSS = SPI_NSS_SOFT;
hspi5.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
hspi5.Init.FirstBit = SPI_FIRSTBIT_MSB;
hspi5.Init.TIMode = SPI_TIMODE_DISABLE;
hspi5.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
hspi5.Init.CRCPolynomial = 0x0;
hspi5.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
hspi5.Init.NSSPolarity = SPI_NSS_POLARITY_LOW;
hspi5.Init.FifoThreshold = SPI_FIFO_THRESHOLD_01DATA;
hspi5.Init.TxCRCInitializationPattern = SPI_CRC_INITIALIZATION_ALL_ZERO_PATTERN;
hspi5.Init.RxCRCInitializationPattern = SPI_CRC_INITIALIZATION_ALL_ZERO_PATTERN;
hspi5.Init.MasterSSIdleness = SPI_MASTER_SS_IDLENESS_00CYCLE;
hspi5.Init.MasterInterDataIdleness = SPI_MASTER_INTERDATA_IDLENESS_00CYCLE;
hspi5.Init.MasterReceiverAutoSusp = SPI_MASTER_RX_AUTOSUSP_DISABLE;
hspi5.Init.MasterKeepIOState = SPI_MASTER_KEEP_IO_STATE_DISABLE;
hspi5.Init.IOSwap = SPI_IO_SWAP_DISABLE;
if (HAL_SPI_Init(&hspi5) != HAL_OK)
{
  Error_Handler();
}


/* main */

{

  /* Clock and Peripheral initialization */
  HAL_NVIC_DisableIRQ(EXTI4_IRQn);
//  MX_DMA_Init();
//  MX_SPI5_Init();
  /* USER CODE BEGIN 2 */
//  GPIO_InitTypeDef GPIO_InitStruct = {0};
//  GPIO_InitStruct.Pin = ADS131M06_CS_GPIO_PIN;
//  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//  GPIO_InitStruct.Pull = GPIO_NOPULL;
//  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
//  HAL_GPIO_Init(ADS131M06_CS_GPIO_PORT, &GPIO_InitStruct);
  /* USER CODE END 2 */
  ads131m06_hardwareInit(&hspi5);
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  ret = ads131m06_adcInit(&hspi5);
  /* Enable the DRDY Interrupt */
  /* Enable Interrupt */
  ads131m06_enableEXTI();
  while (1)
  {
    /* USER CODE END WHILE */
	 if(adc_drdy == 1){
		 adc_drdy = 0;
		 ads131m06_readData(&hspi5);
		 ads_data = (struct adc_data*)((uint8_t*)(hspi5.pTxBuffPtr) + 3);
	 }
    /* USER CODE BEGIN 3 */
  }

}
